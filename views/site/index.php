<?php

/* @var $this yii\web\View */
/* @var $dateTemperature app\models\ArduinoData */
/* @var $dateHumidity app\models\ArduinoData */
/* @var $labels app\models\ArduinoData */

use dosamigos\chartjs\ChartJs;

$this->title = 'Статистика метео станции';
?>
<div class="row">
    <div class="col-lg-12">

        <?php try { ?>
            <?= ChartJs::widget([
                'type' => 'line',
                'options' => [
                    'height' => 'auto',
                    'width' => 400
                ],
                'data' => [
                    'labels' => $labels,
                    'datasets' => [
                        [
                            'label' => "Влажность",
                            'backgroundColor' => "rgba(255,99,132,0.2)",
                            'borderColor' => "rgba(255,99,132,1)",
                            'pointBackgroundColor' => "rgba(255,99,132,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(255,99,132,1)",
                            'data' => $dateHumidity
                        ],
                        [
                            'label' => "Температура",
                            'backgroundColor' => "rgba(119,131,229,0.2)",
                            'borderColor' => "rgba(119,131,229,1)",
                            'pointBackgroundColor' => "rgba(179,181,198,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(179,181,198,1)",
                            'data' => $dateTemperature
                        ]
                    ]
                ]
            ]) ?>
        <?php } catch (Exception $e) {
            var_dump($e);
        } ?>

        <?php $script = <<< JS
$(document).ready(function() {
    setInterval(function(){
        document.location.reload(true);
    }, 30000);
});
JS;
        $this->registerJs($script); ?>

    </div>
</div>
