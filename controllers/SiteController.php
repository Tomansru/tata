<?php

namespace app\controllers;

use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\ArduinoData;

class SiteController extends Controller
{
    /**
     * Установка класса ошибки для обработки нашим шаблоном.
     *
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Отображение домашней страницы.
     *
     * @return string
     */
    public function actionIndex()
    {
        $date = array_reverse(ArduinoData::find()->limit(30)->orderBy(['id' => SORT_DESC])->asArray()->all());

        return $this->render('index', [
            'labels' => ArrayHelper::getColumn($date, 'date'),
            'dateHumidity' => ArrayHelper::getColumn($date, 'humidity'),
            'dateTemperature' => ArrayHelper::getColumn($date, 'temperature')
        ]);
    }
}
