<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 16.03.2018
 * Time: 23:37
 */

namespace app\controllers;

use yii\rest\ActiveController;

class WeatherController extends ActiveController
{
    public $modelClass = 'app\models\ArduinoData';
}