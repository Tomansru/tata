<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 16.03.2018
 * Time: 23:25
 */

namespace app\models;

use \yii\db\ActiveRecord;

class ArduinoData extends ActiveRecord
{
    public function rules()
    {
        return [
            [['temperature', 'humidity'], 'required']
        ];
    }
}